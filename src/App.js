import logo from './logo.svg';
import './App.css';
import HeaderComponent from './components/header.component';

function App() {
  return (
    <div className="App">
     <HeaderComponent />
    </div>
  );
}

export default App;
