import "./header.css"
export default function HeaderComponent () {
    return (
        <div className="header">
            <div className="headertop">
                <div className="headerleft1">
                    <img className="iconmail" src="./iconmail.jpg.png" alt=""/>
                    <p>info@amostudy.com</p>
                </div>
                <div className="headerright1">
                    <div className="menuright1">
                        <img className="chuong" src="./chuong.png" alt=""/>
                        <a>Thông báo</a>
                    </div>
                    <div className="menuright2">
                        <img className="dauhoi" src="./Dấu hỏi@2x.png" alt=""/>
                        <p>Trợ giúp</p>
                    </div>
                    <div className="menuright3">
                        <img className="user" src="./user@2x.png" alt=""/>
                        <p className="dn">Đăng nhập</p>
                        <p className="dk">Đăng ký</p>
                        <p><div className=""></div></p>
                    </div>
                    <div className="menuright4">
                        <img className="facebook" src="./facebook.png" alt=""/>
                        <img className="g+" src="./g+.png" alt=""/>
                        <img className="video" src="./video.png" alt=""/>
                    </div>
                </div>
            </div>
            <div className="headerbottom">
                <div><img className="logo" src="./logo@2x.png" alt=""/></div>  
                <div className="menubottommid">
                    <a>KHÓA HỌC</a>
                    <a>TÀI LIỆU</a>
                    <a>TIN TỨC</a>
                    <a>TRỞ THÀNH GIẢNG VIÊN</a>
                    <a>GIỚI THIỆU</a>
                    <a>LIÊN HỆ</a>
                    </div>  
                    <div className="menubottomend">


                    </div>
            </div>

        </div>
    );
}